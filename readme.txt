A Shunting-yard implementation, takes a math expression in infix notation and returns the same expression in postfix notation.
Inputs need to be separated by a space, "2 * 3" instead of "2*3".
Functions should NOT have a space between the name and the opening bracket, "max("" and not "max (".
Functions with multiple arguments should separate the arguments with a comma, "max( 1 , 3 )"

Example:
ShuntingYard.infixToPostfix("sin( max( 2 , 3 ) / 3 * 3.1415 )") returns "2 3 max 3 / 3.1415 * sin"