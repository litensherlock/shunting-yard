
import static org.junit.Assert.*;
import shunting_yard.ShuntingYard;

/**
 * Created by Jakob L�vhall on 2016-12-31.
 */
public class ShuntingYardTest {
    @org.junit.Test
    public void infixToPostfix() throws Exception {
        String infix = "3 + 4 * 2 / ( 1 - 5 ) ^ 2 ^ 3";
        String postfix = "3 4 2 * 1 5 - 2 3 ^ ^ / +";

        String output = ShuntingYard.infixToPostfix(infix);
        assertEquals(postfix, output);
    }

    @org.junit.Test
    public void function() throws Exception {
        String infix = "sin( max( 2 , 3 ) / 3 * 3.1415 )";
        String postfix = "2 3 max 3 / 3.1415 * sin";

        String output = ShuntingYard.infixToPostfix(infix);
        assertEquals(postfix, output);
    }

    @org.junit.Test
    public void decimals() throws Exception {
        String infix = "3.1 + 4.2 * 2";
        String postfix = "3.1 4.2 2 * +";

        String output = ShuntingYard.infixToPostfix(infix);
        assertEquals(postfix, output);
    }

    @org.junit.Test
    public void modulo() throws Exception {
        String infix = "3 % 2";
        String postfix = "3 2 %";

        String output = ShuntingYard.infixToPostfix(infix);
        assertEquals(postfix, output);
    }
    
    @org.junit.Test
    public void infixToPostfixWithOutSpaces() throws Exception {
        String infix = "3+4*2/(1-5)^2^3";
        String postfix = "3 4 2 * 1 5 - 2 3 ^ ^ / +";
        
        String output = ShuntingYard.infixToPostfix(infix);
        assertEquals(postfix, output);
    }
    
    
    @org.junit.Test
    public void functionWithOutSpaces() throws Exception {
        String infix = "sin(max(2,3)/3*3.1415)";
        String postfix = "2 3 max 3 / 3.1415 * sin";
        
        String output = ShuntingYard.infixToPostfix(infix);
        assertEquals(postfix, output);
    }
}