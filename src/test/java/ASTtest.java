import AST.Node;
import org.junit.Ignore;
import shunting_yard.ShuntingYard;

/**
 * the tests are ignored because they don't check anything automatically,
 * they are meant to be run and inspected
 */
public class ASTtest {
    @org.junit.Test
    @Ignore
    public void infixToPostfix() throws Exception {
        String infix = "3 + 4 * 2 / ( 1 - 5 ) ^ 2 ^ 3";
    
        Node ast = ShuntingYard.infixToAST(infix);
        System.out.println(ast.toString());
    }
    
    @org.junit.Test
    @Ignore
    public void infixToP4ostfix() throws Exception {
        String infix = "2 * ( 1 - 5 ) * 3";
        
        Node ast = ShuntingYard.infixToAST(infix);
        System.out.println(ast.toString());
    }

    @org.junit.Test
    @Ignore
    public void decimals() throws Exception {
        String infix = "3.1 + 4.2 * 2";
    
        Node ast = ShuntingYard.infixToAST(infix);
        System.out.println(ast.toString());
    }

    @org.junit.Test
    @Ignore
    public void modulo() throws Exception {
        String infix = "3 % 2";

        Node ast = ShuntingYard.infixToAST(infix);
        System.out.println(ast.toString());
    }
    
    @org.junit.Test
    @Ignore
    public void functions() throws Exception {
        String infix = "sin( max( 2 , 3 ) / 3 * 3.1415 )";
        
        Node ast = ShuntingYard.infixToAST(infix);
        System.out.println(ast.toString());
    }
    
    @org.junit.Test
    @Ignore
    public void variables() throws Exception {
        String infix = "sin( max( 2 , A ) / 3 * 3.1415 - Q)";
        
        Node ast = ShuntingYard.infixToAST(infix);
        System.out.println(ast.toString());
    }
    
    @org.junit.Test
    @Ignore
    public void prefix1() throws Exception {
        String infix = "+ + 3 - 4 1 * 2 2";
        
        Node ast = ShuntingYard.infixToAST(infix);
        System.out.println(ast.toString());
    }
}