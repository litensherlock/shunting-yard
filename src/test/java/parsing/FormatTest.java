package parsing;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Jakob Lövhall
 * Date: 2017-06-10
 */
public class FormatTest {
    @Test
    public void reduceSpaces() throws Exception {
        String expected = "sin( max( 2 , 3 ) / 3 * 3.1415 )";
        String input  = "sin( max( 2 , 3 )  / 3  *    3.1415  )";
    
        String output = Format.standardise(input);
        assertEquals(expected, output);
    }
    
    @Test
    public void separateNumbers() throws Exception {
        String input = "sin( max(2,3) /3*3.1415)";
        String expected = "sin( max( 2 , 3 ) / 3 * 3.1415 )";
        
        String output = Format.standardise(input);
        assertEquals(expected, output);
    }
    
    @Test
    public void separateOperators() throws Exception {
        String input = "sin( max(2,3)/3*3.1415)";
        String expected = "sin( max( 2 , 3 ) / 3 * 3.1415 )";
        
        String output = Format.standardise(input);
        assertEquals(expected, output);
    }
    
    @Test
    public void separateParenthesis() throws Exception {
        String input = "sin(max(2,3)/3*(3.1415))";
        String expected = "sin( max( 2 , 3 ) / 3 * ( 3.1415 ) )";
        
        String output = Format.standardise(input);
        assertEquals(expected, output);
    }
    
    @Test
    public void separateArgSeparators() throws Exception {
        String input = "pow( max( 2 , 3) ,min( 7 ,2 ))";
        String expected = "pow( max( 2 , 3 ) , min( 7 , 2 ) )";
        
        String output = Format.standardise(input);
        assertEquals(expected, output);
    }
    
    
    
}