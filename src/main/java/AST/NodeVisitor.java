package AST;

/**
 * Jakob Lövhall
 * Date: 2017-12-09
 */
public interface NodeVisitor {
    void visit(Node node);
}
