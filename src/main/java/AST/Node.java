package AST;

import tokens.Token;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.StringJoiner;

/**
 * Jakob Lövhall
 * Date: 2017-11-26
 */
public class Node {
    private Token token;
    private final List<Node> children;
    
    public Node(Token token) {
        this.token = token;
        children = new ArrayList<>(0);
    }
    
    public Token getToken() {
        return token;
    }
    
    public void setToken(final Token token) {
        this.token = token;
    }
    
    public List<Node> getChildren() {
        return children;
    }
    
    public boolean addChild(final Node child){
        return children.add(child);
    }
    
    public void addChildren(final Stack<Node> nodes, int numberOfChildren) {
        final Stack<Node> myStack = new Stack<>();
        for (int i = 0; i < numberOfChildren; i++) {
            myStack.push(nodes.pop());
        }
        for (int i = 0; i < numberOfChildren; i++) {
            children.add(myStack.pop());
        }
        
        nodes.push(this);
    }
    
    public final void depthFirstIteration(final NodeVisitor preOrder, final NodeVisitor inOrder, final NodeVisitor postOrder){
        if (preOrder != null) {
            preOrder.visit(this);
        }
        
        final int mid = children.size() / 2;
        for (int i = 0; i < mid; i++) {
            final Node node = children.get(i);
            node.depthFirstIteration(preOrder, inOrder, postOrder);
        }
        
        if (inOrder != null) {
            inOrder.visit(this);
        }
    
        for (int i = mid; i < children.size(); i++) {
            final Node node = children.get(i);
            node.depthFirstIteration(preOrder, inOrder, postOrder);
        }
        
        if (postOrder != null) {
            postOrder.visit(this);
        }
    }
    
    @Override
    public String toString() {
        final StringJoiner sj = new StringJoiner("");
        internalToString(sj, 0);
        return sj.toString();
    }
    
    private void internalToString(final StringJoiner stringJoiner, int depth) {
        stringJoiner.add("|");
        for (int i = 0; i < depth; i++) {
            stringJoiner.add("--");
        }
        
        stringJoiner.add(" " + token.getString());
        stringJoiner.add(System.lineSeparator());
        if (!children.isEmpty()){
            for (final Node child : children) {
                child.internalToString(stringJoiner, depth + 1);
            }
        }
    }
}
