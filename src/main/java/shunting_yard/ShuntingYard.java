package shunting_yard;

import AST.Node;
import tokens.Token;
import parsing.TokenTokenizer;
import parsing.Tokenizer;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Stack;
import java.util.StringJoiner;

/**
 * Created by Jakob Lövhall on 2016-12-31.
 */
public final class ShuntingYard {
    private ShuntingYard() {}
    
    public static String infixToPostfix(String infix){
        Tokenizer<Token> tokens = new TokenTokenizer(infix);
        Queue<Token> outQueue = new ArrayDeque<>(tokens.length());
        Stack<Token> stack = new Stack<>();

        for (Token token : tokens) {
            token.infixToPostfix(stack, outQueue);
        }

        while (!stack.empty()){
            outQueue.add(stack.pop());
        }

        StringJoiner sj = new StringJoiner(" ");
        for (Token token : outQueue) {
            sj.add(token.getString());
        }

        String postfix = sj.toString();
        return postfix.replace("(", "");
    }
    
    public static Node infixToAST(String infix){
        Tokenizer<Token> tokens = new TokenTokenizer(infix);
        Stack<Node> astStack = new Stack<>();
        Stack<Token> stack = new Stack<>();
        
        for (Token token : tokens) {
            token.infixToAST(stack, astStack);
        }
        
        while (!stack.empty()){
            Node node = new Node(stack.pop());
            Node secondOperand = astStack.pop();
            Node firstOperand = astStack.pop();
            node.addChild(firstOperand);
            node.addChild(secondOperand);
            astStack.push(node);
        }
        
        return astStack.pop();
    }
}
