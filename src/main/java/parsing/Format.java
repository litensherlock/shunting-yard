package parsing;

import java.util.regex.Pattern;

/**
 * Jakob Lövhall
 * Date: 2017-06-10
 */
public class Format {
    private final static Pattern reduceSpaces = Pattern.compile("[\" \"]+");
    private final static Pattern separateOperators = Pattern.compile("[\\+\\-\\*\\/\\^\\%\\!\\,)]");
    private final static Pattern separateNumbers = Pattern.compile("\\d+(\\.\\d+)?");
    private final static Pattern separateStartPar = Pattern.compile("[a-zA-Z]+\\(");
    public static String standardise(String infix){
        String s = separateNumbers.matcher(infix).replaceAll(" $0 ");
        s = separateOperators.matcher(s).replaceAll(" $0 ");
        s = separateStartPar.matcher(s).replaceAll("$0 ");
        String standard = reduceSpaces.matcher(s).replaceAll(" ");
        return standard.trim();
    }
}
