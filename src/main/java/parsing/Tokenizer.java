package parsing;

import java.util.Iterator;

/**
 * Created by Jakob Lövhall on 2016-12-31.
 */
public interface Tokenizer<T> extends Iterable<T>{

    Iterator<T> iterator();
    int length();
}
