package parsing;

import tokens.*;
import tokens.Number;

import java.util.Iterator;

/**
 * Created by Jakob Lövhall on 2016-12-31.
 */
public class TokenTokenizer implements Tokenizer<Token> {

    static String operators = "+-*/^%";
    private String functionStuff = "(),";
    final String[] tokens;

    public TokenTokenizer(String text) {
        String standardise = Format.standardise(text);
        tokens = standardise.split("\\s");
    }

    public Iterator<Token> iterator() {
        return new Iterator<Token>() {
            private int idx = 0;

            public boolean hasNext() {
                return idx < tokens.length;
            }

            public Token next() {
                String text = tokens[idx];
                Token t;
                if (text.length() == 1 && (operators.indexOf(text.charAt(0)) != -1)) {
                    t = new Operator(text);
                } else if (functionStuff.indexOf(text.charAt(text.length() - 1)) != -1){
                    t = new Function(text);
                } else if (Number.isNumber(text)){
                   t = new Number(text);
                } else { //assume variable as default
                    t = new Variable(text);
                }

                idx++;
                return t;
            }
        };
    }

    public int length(){
        return tokens.length;
    }
}
