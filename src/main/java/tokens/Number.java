package tokens;

import AST.Node;

import java.util.Queue;
import java.util.Stack;
import java.util.regex.Pattern;

/**
 * Created by Jakob Lövhall on 2016-12-31.
 */
public class Number implements Token{
    private final static Pattern isNumber = Pattern.compile("-?\\d*(\\.\\d+)?");
    
    public Number(String text) {
        this.text = text;
    }

    private String text;

    @Override
    public String getString() {
        return text;
    }

    @Override
    public void infixToPostfix(Stack<Token> stack, Queue<Token> output) {
        output.add(this);
    }
    
    @Override
    public void infixToAST(Stack<Token> stack, Stack<Node> AstStack) {
        Node node = new Node(this);
        AstStack.push(node);
    }
    
    public static boolean isNumber(String text) {
        return isNumber.matcher(text).find();
    }
}
