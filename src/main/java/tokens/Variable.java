package tokens;

import AST.Node;

import java.util.Queue;
import java.util.Stack;

public class Variable implements Token{
    public Variable(String text) {
        this.text = text;
    }

    private String text;

    @Override
    public String getString() {
        return text;
    }

    @Override
    public void infixToPostfix(Stack<Token> stack, Queue<Token> output) {
        output.add(this);
    }
    
    @Override
    public void infixToAST(Stack<Token> stack, Stack<Node> AstStack) {
        Node node = new Node(this);
        AstStack.push(node);
    }
}
