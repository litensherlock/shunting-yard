package tokens;

import AST.Node;

import java.util.HashMap;
import java.util.Queue;
import java.util.Stack;
import java.util.regex.Pattern;

/**
 * Created by Jakob Lövhall on 2016-12-31.
 */
public class Function implements Token{
    private final Pattern digits = Pattern.compile(".*_(\\d+).*");
    private final String text;
    private final HashMap<String, Integer> numberOfArguments;
    
    public Function(String text) {
        this.text = text;
        numberOfArguments = new HashMap<>();
        numberOfArguments.put("sin", 1);
        numberOfArguments.put("max", 2);
    }
    
    @Override
    public String getString() {
        return text;
    }

    @Override
    public void infixToPostfix(Stack<Token> stack, Queue<Token> output) {
        if (text.charAt(text.length() - 1) == '('){
            stack.push(this);
        } else if (text.charAt(text.length() - 1) == ')'){
            //find the opening bracket
            while (!stack.empty() &&
                    !(stack.peek() instanceof Function)){
                output.add(stack.pop());
            }

            Function f = (Function) stack.pop();

            if(f.getString().length() > 1){ // discard if it is only a opening bracket, otherwise, keep it
                output.add(f);
            }
        } else if ((text.charAt(text.length() - 1) == ',')){

            //find the opening bracket
            while (!stack.empty() &&
                    !(stack.peek() instanceof Function)){
                output.add(stack.pop());
            }
        }
    }
    
    @Override
    public void infixToAST(Stack<Token> stack, Stack<Node> astStack) {
        if (text.charAt(text.length() - 1) == '('){
            stack.push(this);
        } else if (text.charAt(text.length() - 1) == ')'){
            //find the opening bracket
            while (!stack.empty() &&
                    !(stack.peek() instanceof Function)){
                Token token = stack.pop();
                if (token instanceof Operator) {
                    Node node = new Node(token);
                    Node secondOperand = astStack.pop();
                    Node firstOperand = astStack.pop();
                    node.addChild(firstOperand);
                    node.addChild(secondOperand);
                    astStack.push(node);
                }
            }
    
            Function f = (Function) stack.pop();
        
            if(f.getString().length() > 1){ // discard if it is only a opening bracket, otherwise, keep it
                int numberOfArguments = findNumberOfArguments(f);
                Node node = new Node(f);
                node.addChildren(astStack, numberOfArguments);
            }
        }
    }
    
    private int findNumberOfArguments(final Function f) {
        String functionName = f.getString().substring(0, f.getString().length() - 1);
        return numberOfArguments.getOrDefault(functionName, 2);
    }
}
