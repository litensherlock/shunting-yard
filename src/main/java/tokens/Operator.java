package tokens;

import AST.Node;

import java.util.Queue;
import java.util.Stack;

/**
 * Created by Jakob Lövhall on 2016-12-31.
 */
public class Operator implements Token, Comparable<Operator>{
    public Operator(String text) {
        this.text = text;
        if (text.contains("^")){
            rightAssociative = true;
        } else {
            leftAssociative = true;
        }


        String [] levels = {"+-", "*/%", "^"};
        for (int i = 0; i < levels.length; i++) {
            String level = levels[i];
            if (level.contains(text)){
                precedence = i;
                break;
            }
        }
    }

    private int precedence;
    private String text;
    private boolean leftAssociative;
    private boolean rightAssociative;
    
    @Override
    public String getString() {
        return text;
    }

    /**
     * https://en.wikipedia.org/wiki/Shunting-yard_algorithm
     *
     *if the token is an operator, o1, then:
        while there is an operator token o2, at the top of the operator stack and either

         o1 is left-associative and its precedence is less than or equal to that of o2, or
         o1 is right associative, and has precedence less than that of o2,

             pop o2 off the operator stack, onto the output queue;

         at the end of iteration push o1 onto the operator stack
     *
     * @param stack
     * @param output
     */
    @Override
    public void infixToPostfix(Stack<Token> stack, Queue<Token> output) {
        if(this.leftAssociative){
            while (!stack.isEmpty() &&
                    stack.peek() instanceof Operator &&
                    this.compareTo((Operator) stack.peek()) <= 0){
                output.add(stack.pop());
            }
        } else if(this.rightAssociative){
            while (!stack.isEmpty() &&
                    stack.peek() instanceof Operator &&
                    this.compareTo((Operator) stack.peek()) < 0){
                output.add(stack.pop());
            }
        }

        stack.push(this);
    }
    
    @Override
    public void infixToAST(Stack<Token> stack, Stack<Node> astStack) {
        if(this.leftAssociative){
            while (!stack.isEmpty() &&
                    stack.peek() instanceof Operator &&
                    this.compareTo((Operator) stack.peek()) <= 0) {
                Node node = new Node(stack.pop());
                node.addChildren(astStack, 2);
            }
        } else if(this.rightAssociative){
            while (!stack.isEmpty() &&
                    stack.peek() instanceof Operator &&
                    this.compareTo((Operator) stack.peek()) < 0) {
                Node node = new Node(stack.pop());
                node.addChildren(astStack, 2);
            }
        }
        stack.push(this);
    }
    
    @Override
    public int compareTo(Operator o) {
        return precedence - o.precedence;
    }
}
