package tokens;

import AST.Node;

import java.util.Queue;
import java.util.Stack;

/**
 * Created by Jakob Lövhall on 2016-12-31.
 */
public interface Token {
    String getString();
    void infixToPostfix(Stack<Token> stack, Queue<Token> output);
    void infixToAST(Stack<Token> stack, Stack<Node> AstStack);
}
